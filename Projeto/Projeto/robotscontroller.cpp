#include "robotscontroller.h"

RobotsController::RobotsController(Robots *robots,QObject *parent) : QObject(parent), m_robots(robots)
{
    Q_ASSERT(robots!=nullptr);
}

Robot *RobotsController::novoRobot(){
    auto result = m_robots->novoRobot();
    if(result != nullptr){
        result ->setNomeRobot("Novo Robot");
		result->setNomeEquipa("Sem Equipa");
		result->setNomeConfig("Sem Configuracao");
    }
    return result;
}

bool RobotsController:: eliminarRobot(Robot *robot){
    return m_robots->eliminarRobot(robot);
}
