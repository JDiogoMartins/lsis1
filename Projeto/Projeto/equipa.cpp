#include "equipa.h"

Equipa::Equipa(QObject *parent) : QObject(parent)
{

}

QString Equipa::nomeEquipa() const
{
    return m_nomeEquipa;
}

void Equipa::setNomeEquipa(const QString &nomeEquipa)
{
    m_nomeEquipa = nomeEquipa;
}

QString Equipa::nomeEle1() const
{
    return m_nomeEle1;
}

void Equipa::setNomeEle1(const QString &nomeEle1)
{
    m_nomeEle1 = nomeEle1;
}

QString Equipa::nomeEle2() const
{
    return m_nomeEle2;
}

void Equipa::setNomeEle2(const QString &nomeEle2)
{
    m_nomeEle2 = nomeEle2;
}

QString Equipa::nomeEle3() const
{
    return m_nomeEle3;
}

void Equipa::setNomeEle3(const QString &nomeEle3)
{
    m_nomeEle3 = nomeEle3;
}

QString Equipa::nomeEle4() const
{
    return m_nomeEle4;
}

void Equipa::setNomeEle4(const QString &nomeEle4)
{
    m_nomeEle4 = nomeEle4;
}

QString Equipa::nomeEle5() const
{
    return m_nomeEle5;
}

void Equipa::setNomeEle5(const QString &nomeEle5)
{
    m_nomeEle5 = nomeEle5;
}

QString Equipa::nomeEle6() const
{
    return m_nomeEle6;
}

void Equipa::setNomeEle6(const QString &nomeEle6)
{
    m_nomeEle6 = nomeEle6;
}

QString Equipa::nomeEle7() const
{
    return m_nomeEle7;
}

void Equipa::setNomeEle7(const QString &nomeEle7)
{
    m_nomeEle7 = nomeEle7;
}

QString Equipa::nomeEle8() const
{
    return m_nomeEle8;
}

void Equipa::setNomeEle8(const QString &nomeEle8)
{
    m_nomeEle8 = nomeEle8;
}



