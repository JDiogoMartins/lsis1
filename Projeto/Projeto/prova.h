#ifndef PROVA_H
#define PROVA_H

#include <QObject>
#include <QString>
#include <QDate>
#include <QStringList>

class Prova : public QObject
{
	Q_OBJECT


public:
	explicit Prova(QObject *parent = nullptr);

	QString nomeProva() const;
	void setNomeProva(const QString &nomeProva);

	QString LocalProva() const;
	void setLocalProva(const QString &LocalProva);

	QDate DataProva() const;
	void setDataProva(const QDate &DataProva);

signals:

public slots:

private:
	QString m_nomeProva;
	QString m_LocalProva;
	QDate m_DataProva;

};

#endif