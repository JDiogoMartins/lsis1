#ifndef PROVAS_H
#define PROVAS_H

#include "Prova.h"
#include <QObject>
#include <QList>

class Provas : public QObject
{
    Q_OBJECT
public:
    typedef QList<Prova*> ListaProva;

    explicit Provas(QObject *parent = nullptr);

    ListaProva listaProva() const;

	Prova* novaProva();
    bool eliminarProva(Prova *prova);

signals:

    void provaAdded(Prova *prova);
    void provaRemoved(Prova *prova);

    public slots:

private:
    ListaProva m_listaProva;


};

#endif // !PROVAS_H

