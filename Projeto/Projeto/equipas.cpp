#include "equipas.h"

Equipas::Equipas(QObject *parent) : QObject(parent)
{

}

Equipas::ListaEquipa Equipas::listaEquipa() const
{
	return m_listaEquipa;
}

Equipa *Equipas::novaEquipa()
{
	auto result = new Equipa(this);
	if (result != nullptr) {
		m_listaEquipa.append(result);
	}
	return result;
}

bool Equipas::eliminarEquipa(Equipa *equipa)
{
	if (m_listaEquipa.contains(equipa)) {
		emit equipaRemoved(equipa);
		m_listaEquipa.removeOne(equipa);
		delete equipa;
		return true;
	}
	return false;
}
