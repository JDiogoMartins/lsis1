#include "equipascontroller.h"

EquipasController::EquipasController(Equipas *equipas, QObject *parent) : QObject(parent), m_equipas(equipas)
{
    Q_ASSERT(equipas != nullptr);
}

Equipa *EquipasController::novaEquipa()
{
    auto result = m_equipas->novaEquipa();
        if (result != nullptr) {
            result->setNomeEquipa("Nova equipa");
            result->setNomeEle1("Nome Capitão");
            result->setNomeEle2("Desconhecido");
            result->setNomeEle3("Desconhecido");
            result->setNomeEle4("Desconhecido");
            result->setNomeEle5("Desconhecido");
            result->setNomeEle6("Desconhecido");
            result->setNomeEle7("Desconhecido");
            result->setNomeEle8("Desconhecido");
        }
        return result;
}

bool EquipasController::eliminarEquipa(Equipa *equipa)
{
    return m_equipas->eliminarEquipa(equipa);
}
