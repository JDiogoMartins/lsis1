#include "mainwindow.h"
#include <QApplication>
#include <QtSql/qsqldatabase.h>
#include <QtSql/qsqlquery.h>
#include <QMessageBox>
#include <QDebug>
#include "Basedados.h"

int main(int argc, char *argv[])
{

    QApplication a(argc, argv);

	BaseDados bd;

    Provas provas;
    ProvasController ProvasController(&provas);

    Robots robots;
    RobotsController RobotsController(&robots);

    Equipas equipas;
    EquipasController EquipasController(&equipas);

	Configuracoes configs;
	ConfiguracaoController ConfiguracoesController(&configs);

    MainWindow w(&ProvasController, &RobotsController, &EquipasController, &ConfiguracoesController);

    w.show();

    return a.exec();
}
