#pragma once

#include <QObject>
#include <QList>
#include <Configuracao.h>

class Configuracoes : public QObject
{
	Q_OBJECT

public:
	typedef QList<Configuracao*> ListaConfig;

	explicit Configuracoes(QObject *parent = nullptr);

	ListaConfig listaconfi() const;

	Configuracao *novaConfiguracao();
	bool eliminarConfiguracao(Configuracao *config);

signals:
	void configAdded(Configuracao *config);
	void configRemoved(Configuracao *config);

public slots:

private:
	ListaConfig m_listaconfig;
};
