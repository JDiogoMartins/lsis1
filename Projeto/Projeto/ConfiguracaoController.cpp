#include "ConfiguracaoController.h"

ConfiguracaoController::ConfiguracaoController(Configuracoes *configs, QObject *parent) : QObject(parent), m_configs(configs)
{
	Q_ASSERT(configs != nullptr);
}

Configuracao *ConfiguracaoController::novaConfiguracao() {

	auto result = m_configs->novaConfiguracao();
	if (result != nullptr) {
		result->setNomeConfig("Sem nome");
		result->setNSensores("Indefinido");
		result->setMetodo("Desconhecido");
		result->setTempoViragem("Nao definido");
	}
	return result;
}

bool ConfiguracaoController::eliminarConfiguracao(Configuracao *config) {
	return m_configs->eliminarConfiguracao(config);
}