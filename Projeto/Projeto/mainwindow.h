#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QHash>
#include <QString>
#include <QListWidgetItem>
#include <QAbstractButton>
#include "ProvasController.h"
#include "robotscontroller.h"
#include "equipascontroller.h"
#include "ConfiguracaoController.h"
#include "Basedados.h"
#include <QtSerialPort/qserialport.h>
#include <QtSerialPort/qserialportinfo.h>
#include <QtSerialPort/qserialportglobal.h>
#include <QByteArray>


namespace Ui {
class MainWindow;
}

class QSerialPort;

class MainWindow : public QMainWindow
{
	Q_OBJECT

public:
	explicit MainWindow(ProvasController *m_con, RobotsController *m_conR, EquipasController *m_conE, ConfiguracaoController *m_conC, QWidget *parent = 0);
	~MainWindow();

private slots:
	//Prova
	void inserirProva();
	void inserirProvaTabela(Prova *prova);
	void eliminarRowTableProva(Prova *prova);
	void actualizarRowTableProva(QString nomeAntigo, Prova *prova);
	void inserirProvafromBD();
	void modificarProva();
	void eliminarProva();
	void save();
	void discard();
	void reset();
	void on_actionListarProva_triggered();
	void on_actionInsProva_triggered();
	void on_actionModProva_triggered();
	void on_actionEliProva_triggered();
	void on_buttonBox_accepted();
	void on_buttonBox_rejected();
	void on_buttonBox_clicked(QAbstractButton *button);
	void clearProva();
	void on_actionDetProva_triggered();

	//Robot
	void inserirRobot();
	void inserirRobotTabela(Robot *robot);
	void eliminarRowTableRobot(Robot *robot);
	void actulizarRowTableRobot(QString nomeantigo, Robot * robot);
	void inserirRobotfromBD();
	void modificarRobot();
	void eliminarRobot();
	void saveR();
	void discardR();
	void resetR();
	void on_actionListarRobot_triggered();
	void on_actionInsRobot_triggered();
	void on_actionModRobot_triggered();
	void on_actionEliRobot_triggered();
	void on_buttonBoxRobot_accepted();
	void on_buttonBoxRobot_rejected();
	void on_buttonBoxRobot_clicked(QAbstractButton *button);
	void clearRobot();
	void on_actionDetRobot_triggered();

	//Equipa
	void inserirEquipa();
	void inserirEquipaTabela(Equipa *equipa);
	void actualizarRowTableEquipa(QString nomeantigo, Equipa *equipa);
	void eliminarRowTableEquipa(Equipa *equipa);
	void inserirEquipafromBD();
	void modificarEquipa();
	void eliminarEquipa();
	void saveE();
	void discardE();
	void resetE();
	void on_actionListarEquipa_triggered();
	void on_actionInsEquipa_triggered();
	void on_actionModEquipa_triggered();
	void on_actionEliEquipa_triggered();
	void on_buttonBoxEquipa_accepted();
	void on_buttonBoxEquipa_rejected();
	void on_buttonBoxEquipa_clicked(QAbstractButton *button);
	void clearEquipa();
	void on_actionDetEquipa_triggered();


	//Configuracao
	void inserirConfig();
	void inserirConfigTabela(Configuracao *config);
	void actulizarRowTableConfig(QString nomeantigo, Configuracao *config);
	void eliminarRowTableConfig(Configuracao *config);
	void inserirConfigfromBD();
	void modificarConfiguracao();
	void eliminarConfiguracao();
	void saveC();
	void discardC();
	void resetC();
	void clearConfig();
	void on_actionLisConfi_triggered();
	void on_actionDetConfig_triggered();
	void on_actionInsConfig_triggered();
	void on_actionModConfig_triggered();
	void on_actionEliConfig_triggered();
	void on_buttonBoxConfig_accepted();
	void on_buttonBoxConfig_rejected();
	void on_buttonBoxConfig_clicked(QAbstractButton *button);

	//Modo Prova
	void ModoProva(int fase);
	void on_actionIniciar_Prova_triggered();
	void on_pushButton_clicked();
	void Fases();
	void receberFases();

	//Arduino
	void readSerial();
	void updateEstados(int);

private:
	Ui::MainWindow *ui;
	QTimer *timer;

	//Arduino 
	QSerialPort *arduino;
	static const quint16 arduino_uno_vendor_id = 1027;
	static const quint16 arduino_uno_product_id = 24577;
	QByteArray serialData;
	QString serialBuffer;
	QString parsed_data;
	int estado_valor;

	vector<int>  estados{1, 2, 3, 4, 5, 6, 7, 7, 6, 5, 4, 3, 2, 1};

	BaseDados bd;
    ProvasController *m_Pcontroller;
    RobotsController *m_Rcontroller;
    EquipasController *m_Econtroller;
	ConfiguracaoController *m_Ccontroller;
    QHash<QListWidgetItem*, Prova*> m_PentryMap;
    QHash<QListWidgetItem*, Robot*> m_RentryMap;
    QHash<QListWidgetItem*, Equipa*> m_EentryMap;
	QHash<QListWidgetItem*, Configuracao*> m_CentryMap;
};

#endif // MAINWINDOW_H
