#ifndef EQUIPASCONTROLLER_H
#define EQUIPASCONTROLLER_H

#include <QObject>
#include <equipas.h>

class EquipasController : public QObject
{
    Q_OBJECT

public:
    explicit EquipasController(Equipas *equipas, QObject *parent = nullptr);

    Equipa *novaEquipa();
    bool eliminarEquipa(Equipa *equipa);

signals:

public slots:

private:
    Equipas *m_equipas;
};

#endif // EQUIPASCONTROLLER_H
