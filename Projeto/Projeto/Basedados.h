#pragma once
#pragma once

#ifndef BaseDados_
#define BaseDados_
#endif;

#include<iostream>
#include<string>
#include<exception>

#include<mysql_connection.h>
#include<mysql_driver.h>
#include <Windows.h>

#include<cppconn/driver.h>
#include<cppconn/exception.h>
#include<cppconn/resultset.h>
#include<cppconn/statement.h>
#include<cppconn/sqlstring.h>
#include<qmessagebox.h>




using namespace std;
using namespace sql;

class BaseDados {
private:

	sql::Driver *driver;
	sql::Connection *con;
	sql::Statement *stmt;
	sql::ResultSet *res;
	sql::RowID *row;
	sql::SQLString mysql;


public:
	void Ligar();
	void desligar();

	//Prova
	int getIDProva(string nomeProva, string localProva);
	void inserirProvaBD(string nomeProva, string localProva, string data);
	void inserirProvaLista(vector<string>* , vector<string>*, vector<string>*);
	void eliminarProvaBD(string nomeProva, string localProva);
	void modificarProvaBD(int id, string nomeProva, string localProva, string data);

	//Equipa
	int getIDEquipa(string nomeEquipa);
	string getNomeEquipa(int idEquipa);
	void inserirEquipaLista(vector<string>*, vector<string>*, vector<string>*, vector<string>*, vector<string>*, vector<string>*, vector<string>*, vector<string>*, vector<string>*);
	void inserirEquipaBD(string nomeEquipa, string nEle1, string nEle2, string nEle3, string nEle4, string nEle5, string nEle6, string nEle7, string nEle8);
	void eliminarEquipaBD(string nomeEquipa);
	void modificarEquipaBD(int id, string nomeEquipa, string nEle1, string nEle2, string nEle3, string nEle4, string nEle5, string nEle6, string nEle7, string nEle8);

	//Robot
	int getIDRobot(string nomeRobot, string nomeEquipa, string nomeConfig);
	void inserirRobotBD(string nomeRobot, string nomeEquipa, string nomeConfig);
	void inserirRobotLista(vector<string> *nomesR, vector<string> *nomesE, vector<string> *nomesC);
	void eliminarRobotBD(string nomeRobot, string nomeEquipa, string nomeC);
	void modificarRobotBD(int id, string nomeRobot, string nomeEquipa, string nomeC);

	//Configuracao
	int getIDConfig(string nomeConfig);
	string getNomeConfig(int idConfig);
	void inserirConfigLista(vector<string>*, vector<string>*, vector<string>*, vector<string>*);
	void inserirConfigBD(string nomeConfig, string tempoV, string nSensores, string metodo);
	void eliminarConfigBD(string nomeConfig, string nSensores, string tempoV, string metodo);
	void modificarConfigBD(int id, string nomeConfig, string tempoV, string nSensores, string metodo);

};

