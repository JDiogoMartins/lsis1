/********************************************************************************
** Form generated from reading UI file 'mainwindow.ui'
**
** Created by: Qt User Interface Compiler version 5.11.0
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_MAINWINDOW_H
#define UI_MAINWINDOW_H

#include <QtCore/QVariant>
#include <QtWidgets/QAction>
#include <QtWidgets/QApplication>
#include <QtWidgets/QComboBox>
#include <QtWidgets/QDateEdit>
#include <QtWidgets/QDialogButtonBox>
#include <QtWidgets/QGridLayout>
#include <QtWidgets/QHeaderView>
#include <QtWidgets/QLabel>
#include <QtWidgets/QLineEdit>
#include <QtWidgets/QListWidget>
#include <QtWidgets/QMainWindow>
#include <QtWidgets/QMenu>
#include <QtWidgets/QMenuBar>
#include <QtWidgets/QPushButton>
#include <QtWidgets/QStackedWidget>
#include <QtWidgets/QStatusBar>
#include <QtWidgets/QTableWidget>
#include <QtWidgets/QWidget>

QT_BEGIN_NAMESPACE

class Ui_MainWindow
{
public:
    QAction *actionInsProva;
    QAction *actionModProva;
    QAction *actionEliProva;
    QAction *actionInsRobot;
    QAction *actionModRobot;
    QAction *actionEliRobot;
    QAction *actionListarProva;
    QAction *actionListarRobot;
    QAction *actionListarEquipa;
    QAction *actionInsEquipa;
    QAction *actionModEquipa;
    QAction *actionEliEquipa;
    QAction *actionDetProva;
    QAction *actionDetEquipa;
    QAction *actionDetRobot;
    QAction *actionLisConfi;
    QAction *actionDetConfig;
    QAction *actionInsConfig;
    QAction *actionModConfig;
    QAction *actionEliConfig;
    QAction *actionIniciar_Prova;
    QWidget *centralWidget;
    QGridLayout *gridLayout;
    QStackedWidget *stackedWidget;
    QWidget *Principal;
    QGridLayout *gridLayout_2;
    QLabel *label;
    QWidget *pageTableProva;
    QGridLayout *gridLayout_7;
    QTableWidget *tableProva;
    QWidget *pageList;
    QGridLayout *gridLayout_4;
    QListWidget *listWidget;
    QWidget *pageInserirProva;
    QGridLayout *gridLayout_3;
    QLabel *label_2;
    QLineEdit *nomeEdit;
    QLabel *label_3;
    QLineEdit *localEdit;
    QLabel *label_4;
    QDateEdit *dateEdit;
    QDialogButtonBox *buttonBox;
    QWidget *pageListRobots;
    QGridLayout *gridLayout_5;
    QListWidget *listRobots;
    QWidget *pageTableRobot;
    QGridLayout *gridLayout_10;
    QTableWidget *tableRobot;
    QWidget *pageInserirRobot;
    QGridLayout *gridLayout_6;
    QLabel *label_5;
    QLineEdit *nomeRobotEdit;
    QLabel *label_6;
    QComboBox *comboBoxEquipa;
    QLabel *label_7;
    QComboBox *comboBoxConfig;
    QDialogButtonBox *buttonBoxRobot;
    QWidget *pageListEquipa;
    QGridLayout *gridLayout_8;
    QListWidget *listEquipa;
    QWidget *pageTableEquipa;
    QGridLayout *gridLayout_11;
    QTableWidget *tableEquipa;
    QWidget *pageInserirEquipa;
    QGridLayout *gridLayout_9;
    QDialogButtonBox *buttonBoxEquipa;
    QLabel *label_9;
    QLabel *label_11;
    QLabel *label_16;
    QLineEdit *nomeEle2Edit;
    QLineEdit *nomeEle6Edit;
    QLabel *label_15;
    QLineEdit *nomeEle7Edit;
    QLabel *label_12;
    QLabel *label_14;
    QLineEdit *nomeEle1Edit;
    QLineEdit *nomeEle8Edit;
    QLabel *label_8;
    QLineEdit *nomeEquipaEdit;
    QLabel *label_13;
    QLabel *label_10;
    QLineEdit *nomeEle5Edit;
    QLineEdit *nomeEle3Edit;
    QLineEdit *nomeEle4Edit;
    QWidget *pageListConfig;
    QGridLayout *gridLayout_12;
    QListWidget *listConfig;
    QWidget *pageTableConfig;
    QGridLayout *gridLayout_13;
    QTableWidget *tableConfig;
    QWidget *pageInsertConfig;
    QGridLayout *gridLayout_14;
    QComboBox *metodoEdit;
    QLineEdit *tempoViragemEdit;
    QComboBox *nSensoresEdit;
    QLabel *label_20;
    QLabel *label_19;
    QLabel *label_17;
    QLineEdit *nomeConfigEdit;
    QLabel *label_18;
    QDialogButtonBox *buttonBoxConfig;
    QWidget *pageProva;
    QPushButton *pushEsquerda;
    QPushButton *pushVentoinha;
    QPushButton *pushParado;
    QPushButton *pushLED;
    QPushButton *pushDireita;
    QPushButton *pushFrente;
    QPushButton *pushButton;
    QMenuBar *menuBar;
    QMenu *menuMenu;
    QMenu *menuInserir;
    QMenu *menuModificar;
    QMenu *menuEliminar;
    QMenu *menuListar;
    QMenu *menuListar_Detalhado;
    QMenu *menuModo_Prova;
    QStatusBar *statusBar;

    void setupUi(QMainWindow *MainWindow)
    {
        if (MainWindow->objectName().isEmpty())
            MainWindow->setObjectName(QStringLiteral("MainWindow"));
        MainWindow->resize(588, 311);
        actionInsProva = new QAction(MainWindow);
        actionInsProva->setObjectName(QStringLiteral("actionInsProva"));
        actionModProva = new QAction(MainWindow);
        actionModProva->setObjectName(QStringLiteral("actionModProva"));
        actionEliProva = new QAction(MainWindow);
        actionEliProva->setObjectName(QStringLiteral("actionEliProva"));
        actionInsRobot = new QAction(MainWindow);
        actionInsRobot->setObjectName(QStringLiteral("actionInsRobot"));
        actionModRobot = new QAction(MainWindow);
        actionModRobot->setObjectName(QStringLiteral("actionModRobot"));
        actionEliRobot = new QAction(MainWindow);
        actionEliRobot->setObjectName(QStringLiteral("actionEliRobot"));
        actionListarProva = new QAction(MainWindow);
        actionListarProva->setObjectName(QStringLiteral("actionListarProva"));
        actionListarRobot = new QAction(MainWindow);
        actionListarRobot->setObjectName(QStringLiteral("actionListarRobot"));
        actionListarEquipa = new QAction(MainWindow);
        actionListarEquipa->setObjectName(QStringLiteral("actionListarEquipa"));
        actionInsEquipa = new QAction(MainWindow);
        actionInsEquipa->setObjectName(QStringLiteral("actionInsEquipa"));
        actionModEquipa = new QAction(MainWindow);
        actionModEquipa->setObjectName(QStringLiteral("actionModEquipa"));
        actionEliEquipa = new QAction(MainWindow);
        actionEliEquipa->setObjectName(QStringLiteral("actionEliEquipa"));
        actionDetProva = new QAction(MainWindow);
        actionDetProva->setObjectName(QStringLiteral("actionDetProva"));
        actionDetEquipa = new QAction(MainWindow);
        actionDetEquipa->setObjectName(QStringLiteral("actionDetEquipa"));
        actionDetRobot = new QAction(MainWindow);
        actionDetRobot->setObjectName(QStringLiteral("actionDetRobot"));
        actionLisConfi = new QAction(MainWindow);
        actionLisConfi->setObjectName(QStringLiteral("actionLisConfi"));
        actionDetConfig = new QAction(MainWindow);
        actionDetConfig->setObjectName(QStringLiteral("actionDetConfig"));
        actionInsConfig = new QAction(MainWindow);
        actionInsConfig->setObjectName(QStringLiteral("actionInsConfig"));
        actionModConfig = new QAction(MainWindow);
        actionModConfig->setObjectName(QStringLiteral("actionModConfig"));
        actionEliConfig = new QAction(MainWindow);
        actionEliConfig->setObjectName(QStringLiteral("actionEliConfig"));
        actionIniciar_Prova = new QAction(MainWindow);
        actionIniciar_Prova->setObjectName(QStringLiteral("actionIniciar_Prova"));
        centralWidget = new QWidget(MainWindow);
        centralWidget->setObjectName(QStringLiteral("centralWidget"));
        gridLayout = new QGridLayout(centralWidget);
        gridLayout->setSpacing(6);
        gridLayout->setContentsMargins(11, 11, 11, 11);
        gridLayout->setObjectName(QStringLiteral("gridLayout"));
        stackedWidget = new QStackedWidget(centralWidget);
        stackedWidget->setObjectName(QStringLiteral("stackedWidget"));
        Principal = new QWidget();
        Principal->setObjectName(QStringLiteral("Principal"));
        gridLayout_2 = new QGridLayout(Principal);
        gridLayout_2->setSpacing(6);
        gridLayout_2->setContentsMargins(11, 11, 11, 11);
        gridLayout_2->setObjectName(QStringLiteral("gridLayout_2"));
        label = new QLabel(Principal);
        label->setObjectName(QStringLiteral("label"));

        gridLayout_2->addWidget(label, 0, 0, 1, 1);

        stackedWidget->addWidget(Principal);
        pageTableProva = new QWidget();
        pageTableProva->setObjectName(QStringLiteral("pageTableProva"));
        gridLayout_7 = new QGridLayout(pageTableProva);
        gridLayout_7->setSpacing(6);
        gridLayout_7->setContentsMargins(11, 11, 11, 11);
        gridLayout_7->setObjectName(QStringLiteral("gridLayout_7"));
        tableProva = new QTableWidget(pageTableProva);
        if (tableProva->columnCount() < 3)
            tableProva->setColumnCount(3);
        QTableWidgetItem *__qtablewidgetitem = new QTableWidgetItem();
        tableProva->setHorizontalHeaderItem(0, __qtablewidgetitem);
        QTableWidgetItem *__qtablewidgetitem1 = new QTableWidgetItem();
        tableProva->setHorizontalHeaderItem(1, __qtablewidgetitem1);
        QTableWidgetItem *__qtablewidgetitem2 = new QTableWidgetItem();
        tableProva->setHorizontalHeaderItem(2, __qtablewidgetitem2);
        tableProva->setObjectName(QStringLiteral("tableProva"));
        tableProva->setAlternatingRowColors(true);
        tableProva->setSelectionBehavior(QAbstractItemView::SelectRows);

        gridLayout_7->addWidget(tableProva, 0, 0, 1, 1);

        stackedWidget->addWidget(pageTableProva);
        pageList = new QWidget();
        pageList->setObjectName(QStringLiteral("pageList"));
        gridLayout_4 = new QGridLayout(pageList);
        gridLayout_4->setSpacing(6);
        gridLayout_4->setContentsMargins(11, 11, 11, 11);
        gridLayout_4->setObjectName(QStringLiteral("gridLayout_4"));
        listWidget = new QListWidget(pageList);
        listWidget->setObjectName(QStringLiteral("listWidget"));

        gridLayout_4->addWidget(listWidget, 0, 0, 1, 1);

        stackedWidget->addWidget(pageList);
        pageInserirProva = new QWidget();
        pageInserirProva->setObjectName(QStringLiteral("pageInserirProva"));
        gridLayout_3 = new QGridLayout(pageInserirProva);
        gridLayout_3->setSpacing(6);
        gridLayout_3->setContentsMargins(11, 11, 11, 11);
        gridLayout_3->setObjectName(QStringLiteral("gridLayout_3"));
        label_2 = new QLabel(pageInserirProva);
        label_2->setObjectName(QStringLiteral("label_2"));

        gridLayout_3->addWidget(label_2, 0, 0, 1, 1);

        nomeEdit = new QLineEdit(pageInserirProva);
        nomeEdit->setObjectName(QStringLiteral("nomeEdit"));

        gridLayout_3->addWidget(nomeEdit, 0, 1, 1, 1);

        label_3 = new QLabel(pageInserirProva);
        label_3->setObjectName(QStringLiteral("label_3"));

        gridLayout_3->addWidget(label_3, 1, 0, 1, 1);

        localEdit = new QLineEdit(pageInserirProva);
        localEdit->setObjectName(QStringLiteral("localEdit"));

        gridLayout_3->addWidget(localEdit, 1, 1, 1, 1);

        label_4 = new QLabel(pageInserirProva);
        label_4->setObjectName(QStringLiteral("label_4"));

        gridLayout_3->addWidget(label_4, 2, 0, 1, 1);

        dateEdit = new QDateEdit(pageInserirProva);
        dateEdit->setObjectName(QStringLiteral("dateEdit"));

        gridLayout_3->addWidget(dateEdit, 2, 1, 1, 1);

        buttonBox = new QDialogButtonBox(pageInserirProva);
        buttonBox->setObjectName(QStringLiteral("buttonBox"));
        buttonBox->setStandardButtons(QDialogButtonBox::Cancel|QDialogButtonBox::Reset|QDialogButtonBox::Save);

        gridLayout_3->addWidget(buttonBox, 3, 0, 1, 2);

        stackedWidget->addWidget(pageInserirProva);
        pageListRobots = new QWidget();
        pageListRobots->setObjectName(QStringLiteral("pageListRobots"));
        gridLayout_5 = new QGridLayout(pageListRobots);
        gridLayout_5->setSpacing(6);
        gridLayout_5->setContentsMargins(11, 11, 11, 11);
        gridLayout_5->setObjectName(QStringLiteral("gridLayout_5"));
        listRobots = new QListWidget(pageListRobots);
        listRobots->setObjectName(QStringLiteral("listRobots"));

        gridLayout_5->addWidget(listRobots, 0, 0, 1, 1);

        stackedWidget->addWidget(pageListRobots);
        pageTableRobot = new QWidget();
        pageTableRobot->setObjectName(QStringLiteral("pageTableRobot"));
        gridLayout_10 = new QGridLayout(pageTableRobot);
        gridLayout_10->setSpacing(6);
        gridLayout_10->setContentsMargins(11, 11, 11, 11);
        gridLayout_10->setObjectName(QStringLiteral("gridLayout_10"));
        tableRobot = new QTableWidget(pageTableRobot);
        if (tableRobot->columnCount() < 3)
            tableRobot->setColumnCount(3);
        QTableWidgetItem *__qtablewidgetitem3 = new QTableWidgetItem();
        tableRobot->setHorizontalHeaderItem(0, __qtablewidgetitem3);
        QTableWidgetItem *__qtablewidgetitem4 = new QTableWidgetItem();
        tableRobot->setHorizontalHeaderItem(1, __qtablewidgetitem4);
        QTableWidgetItem *__qtablewidgetitem5 = new QTableWidgetItem();
        tableRobot->setHorizontalHeaderItem(2, __qtablewidgetitem5);
        tableRobot->setObjectName(QStringLiteral("tableRobot"));
        tableRobot->setSelectionBehavior(QAbstractItemView::SelectRows);

        gridLayout_10->addWidget(tableRobot, 0, 0, 1, 1);

        stackedWidget->addWidget(pageTableRobot);
        pageInserirRobot = new QWidget();
        pageInserirRobot->setObjectName(QStringLiteral("pageInserirRobot"));
        gridLayout_6 = new QGridLayout(pageInserirRobot);
        gridLayout_6->setSpacing(6);
        gridLayout_6->setContentsMargins(11, 11, 11, 11);
        gridLayout_6->setObjectName(QStringLiteral("gridLayout_6"));
        label_5 = new QLabel(pageInserirRobot);
        label_5->setObjectName(QStringLiteral("label_5"));

        gridLayout_6->addWidget(label_5, 0, 0, 1, 1);

        nomeRobotEdit = new QLineEdit(pageInserirRobot);
        nomeRobotEdit->setObjectName(QStringLiteral("nomeRobotEdit"));

        gridLayout_6->addWidget(nomeRobotEdit, 0, 1, 1, 1);

        label_6 = new QLabel(pageInserirRobot);
        label_6->setObjectName(QStringLiteral("label_6"));

        gridLayout_6->addWidget(label_6, 1, 0, 1, 1);

        comboBoxEquipa = new QComboBox(pageInserirRobot);
        comboBoxEquipa->setObjectName(QStringLiteral("comboBoxEquipa"));

        gridLayout_6->addWidget(comboBoxEquipa, 1, 1, 1, 1);

        label_7 = new QLabel(pageInserirRobot);
        label_7->setObjectName(QStringLiteral("label_7"));

        gridLayout_6->addWidget(label_7, 2, 0, 1, 1);

        comboBoxConfig = new QComboBox(pageInserirRobot);
        comboBoxConfig->setObjectName(QStringLiteral("comboBoxConfig"));

        gridLayout_6->addWidget(comboBoxConfig, 2, 1, 1, 1);

        buttonBoxRobot = new QDialogButtonBox(pageInserirRobot);
        buttonBoxRobot->setObjectName(QStringLiteral("buttonBoxRobot"));
        buttonBoxRobot->setStandardButtons(QDialogButtonBox::Cancel|QDialogButtonBox::Reset|QDialogButtonBox::Save);

        gridLayout_6->addWidget(buttonBoxRobot, 3, 0, 1, 2);

        stackedWidget->addWidget(pageInserirRobot);
        pageListEquipa = new QWidget();
        pageListEquipa->setObjectName(QStringLiteral("pageListEquipa"));
        gridLayout_8 = new QGridLayout(pageListEquipa);
        gridLayout_8->setSpacing(6);
        gridLayout_8->setContentsMargins(11, 11, 11, 11);
        gridLayout_8->setObjectName(QStringLiteral("gridLayout_8"));
        listEquipa = new QListWidget(pageListEquipa);
        listEquipa->setObjectName(QStringLiteral("listEquipa"));

        gridLayout_8->addWidget(listEquipa, 0, 0, 1, 1);

        stackedWidget->addWidget(pageListEquipa);
        pageTableEquipa = new QWidget();
        pageTableEquipa->setObjectName(QStringLiteral("pageTableEquipa"));
        gridLayout_11 = new QGridLayout(pageTableEquipa);
        gridLayout_11->setSpacing(6);
        gridLayout_11->setContentsMargins(11, 11, 11, 11);
        gridLayout_11->setObjectName(QStringLiteral("gridLayout_11"));
        tableEquipa = new QTableWidget(pageTableEquipa);
        if (tableEquipa->columnCount() < 9)
            tableEquipa->setColumnCount(9);
        QTableWidgetItem *__qtablewidgetitem6 = new QTableWidgetItem();
        tableEquipa->setHorizontalHeaderItem(0, __qtablewidgetitem6);
        QTableWidgetItem *__qtablewidgetitem7 = new QTableWidgetItem();
        tableEquipa->setHorizontalHeaderItem(1, __qtablewidgetitem7);
        QTableWidgetItem *__qtablewidgetitem8 = new QTableWidgetItem();
        tableEquipa->setHorizontalHeaderItem(2, __qtablewidgetitem8);
        QTableWidgetItem *__qtablewidgetitem9 = new QTableWidgetItem();
        tableEquipa->setHorizontalHeaderItem(3, __qtablewidgetitem9);
        QTableWidgetItem *__qtablewidgetitem10 = new QTableWidgetItem();
        tableEquipa->setHorizontalHeaderItem(4, __qtablewidgetitem10);
        QTableWidgetItem *__qtablewidgetitem11 = new QTableWidgetItem();
        tableEquipa->setHorizontalHeaderItem(5, __qtablewidgetitem11);
        QTableWidgetItem *__qtablewidgetitem12 = new QTableWidgetItem();
        tableEquipa->setHorizontalHeaderItem(6, __qtablewidgetitem12);
        QTableWidgetItem *__qtablewidgetitem13 = new QTableWidgetItem();
        tableEquipa->setHorizontalHeaderItem(7, __qtablewidgetitem13);
        QTableWidgetItem *__qtablewidgetitem14 = new QTableWidgetItem();
        tableEquipa->setHorizontalHeaderItem(8, __qtablewidgetitem14);
        tableEquipa->setObjectName(QStringLiteral("tableEquipa"));
        tableEquipa->setSelectionBehavior(QAbstractItemView::SelectRows);

        gridLayout_11->addWidget(tableEquipa, 0, 0, 1, 1);

        stackedWidget->addWidget(pageTableEquipa);
        pageInserirEquipa = new QWidget();
        pageInserirEquipa->setObjectName(QStringLiteral("pageInserirEquipa"));
        gridLayout_9 = new QGridLayout(pageInserirEquipa);
        gridLayout_9->setSpacing(6);
        gridLayout_9->setContentsMargins(11, 11, 11, 11);
        gridLayout_9->setObjectName(QStringLiteral("gridLayout_9"));
        buttonBoxEquipa = new QDialogButtonBox(pageInserirEquipa);
        buttonBoxEquipa->setObjectName(QStringLiteral("buttonBoxEquipa"));
        buttonBoxEquipa->setStandardButtons(QDialogButtonBox::Cancel|QDialogButtonBox::Reset|QDialogButtonBox::Save);

        gridLayout_9->addWidget(buttonBoxEquipa, 6, 0, 1, 5);

        label_9 = new QLabel(pageInserirEquipa);
        label_9->setObjectName(QStringLiteral("label_9"));

        gridLayout_9->addWidget(label_9, 2, 0, 1, 1);

        label_11 = new QLabel(pageInserirEquipa);
        label_11->setObjectName(QStringLiteral("label_11"));

        gridLayout_9->addWidget(label_11, 4, 0, 1, 2);

        label_16 = new QLabel(pageInserirEquipa);
        label_16->setObjectName(QStringLiteral("label_16"));

        gridLayout_9->addWidget(label_16, 5, 3, 1, 1);

        nomeEle2Edit = new QLineEdit(pageInserirEquipa);
        nomeEle2Edit->setObjectName(QStringLiteral("nomeEle2Edit"));

        gridLayout_9->addWidget(nomeEle2Edit, 3, 2, 1, 1);

        nomeEle6Edit = new QLineEdit(pageInserirEquipa);
        nomeEle6Edit->setObjectName(QStringLiteral("nomeEle6Edit"));

        gridLayout_9->addWidget(nomeEle6Edit, 3, 4, 1, 1);

        label_15 = new QLabel(pageInserirEquipa);
        label_15->setObjectName(QStringLiteral("label_15"));

        gridLayout_9->addWidget(label_15, 4, 3, 1, 1);

        nomeEle7Edit = new QLineEdit(pageInserirEquipa);
        nomeEle7Edit->setObjectName(QStringLiteral("nomeEle7Edit"));

        gridLayout_9->addWidget(nomeEle7Edit, 4, 4, 1, 1);

        label_12 = new QLabel(pageInserirEquipa);
        label_12->setObjectName(QStringLiteral("label_12"));

        gridLayout_9->addWidget(label_12, 5, 0, 1, 1);

        label_14 = new QLabel(pageInserirEquipa);
        label_14->setObjectName(QStringLiteral("label_14"));

        gridLayout_9->addWidget(label_14, 3, 3, 1, 1);

        nomeEle1Edit = new QLineEdit(pageInserirEquipa);
        nomeEle1Edit->setObjectName(QStringLiteral("nomeEle1Edit"));

        gridLayout_9->addWidget(nomeEle1Edit, 2, 2, 1, 1);

        nomeEle8Edit = new QLineEdit(pageInserirEquipa);
        nomeEle8Edit->setObjectName(QStringLiteral("nomeEle8Edit"));

        gridLayout_9->addWidget(nomeEle8Edit, 5, 4, 1, 1);

        label_8 = new QLabel(pageInserirEquipa);
        label_8->setObjectName(QStringLiteral("label_8"));

        gridLayout_9->addWidget(label_8, 0, 0, 1, 2);

        nomeEquipaEdit = new QLineEdit(pageInserirEquipa);
        nomeEquipaEdit->setObjectName(QStringLiteral("nomeEquipaEdit"));

        gridLayout_9->addWidget(nomeEquipaEdit, 0, 2, 1, 1);

        label_13 = new QLabel(pageInserirEquipa);
        label_13->setObjectName(QStringLiteral("label_13"));

        gridLayout_9->addWidget(label_13, 2, 3, 1, 1);

        label_10 = new QLabel(pageInserirEquipa);
        label_10->setObjectName(QStringLiteral("label_10"));

        gridLayout_9->addWidget(label_10, 3, 0, 1, 1);

        nomeEle5Edit = new QLineEdit(pageInserirEquipa);
        nomeEle5Edit->setObjectName(QStringLiteral("nomeEle5Edit"));

        gridLayout_9->addWidget(nomeEle5Edit, 2, 4, 1, 1);

        nomeEle3Edit = new QLineEdit(pageInserirEquipa);
        nomeEle3Edit->setObjectName(QStringLiteral("nomeEle3Edit"));

        gridLayout_9->addWidget(nomeEle3Edit, 4, 2, 1, 1);

        nomeEle4Edit = new QLineEdit(pageInserirEquipa);
        nomeEle4Edit->setObjectName(QStringLiteral("nomeEle4Edit"));

        gridLayout_9->addWidget(nomeEle4Edit, 5, 2, 1, 1);

        stackedWidget->addWidget(pageInserirEquipa);
        pageListConfig = new QWidget();
        pageListConfig->setObjectName(QStringLiteral("pageListConfig"));
        gridLayout_12 = new QGridLayout(pageListConfig);
        gridLayout_12->setSpacing(6);
        gridLayout_12->setContentsMargins(11, 11, 11, 11);
        gridLayout_12->setObjectName(QStringLiteral("gridLayout_12"));
        listConfig = new QListWidget(pageListConfig);
        listConfig->setObjectName(QStringLiteral("listConfig"));

        gridLayout_12->addWidget(listConfig, 0, 0, 1, 1);

        stackedWidget->addWidget(pageListConfig);
        pageTableConfig = new QWidget();
        pageTableConfig->setObjectName(QStringLiteral("pageTableConfig"));
        gridLayout_13 = new QGridLayout(pageTableConfig);
        gridLayout_13->setSpacing(6);
        gridLayout_13->setContentsMargins(11, 11, 11, 11);
        gridLayout_13->setObjectName(QStringLiteral("gridLayout_13"));
        tableConfig = new QTableWidget(pageTableConfig);
        if (tableConfig->columnCount() < 4)
            tableConfig->setColumnCount(4);
        QTableWidgetItem *__qtablewidgetitem15 = new QTableWidgetItem();
        tableConfig->setHorizontalHeaderItem(0, __qtablewidgetitem15);
        QTableWidgetItem *__qtablewidgetitem16 = new QTableWidgetItem();
        tableConfig->setHorizontalHeaderItem(1, __qtablewidgetitem16);
        QTableWidgetItem *__qtablewidgetitem17 = new QTableWidgetItem();
        tableConfig->setHorizontalHeaderItem(2, __qtablewidgetitem17);
        QTableWidgetItem *__qtablewidgetitem18 = new QTableWidgetItem();
        tableConfig->setHorizontalHeaderItem(3, __qtablewidgetitem18);
        tableConfig->setObjectName(QStringLiteral("tableConfig"));
        tableConfig->setMinimumSize(QSize(546, 234));
        tableConfig->setMaximumSize(QSize(546, 234));
        tableConfig->setSelectionBehavior(QAbstractItemView::SelectRows);

        gridLayout_13->addWidget(tableConfig, 0, 0, 1, 1);

        stackedWidget->addWidget(pageTableConfig);
        pageInsertConfig = new QWidget();
        pageInsertConfig->setObjectName(QStringLiteral("pageInsertConfig"));
        gridLayout_14 = new QGridLayout(pageInsertConfig);
        gridLayout_14->setSpacing(6);
        gridLayout_14->setContentsMargins(11, 11, 11, 11);
        gridLayout_14->setObjectName(QStringLiteral("gridLayout_14"));
        metodoEdit = new QComboBox(pageInsertConfig);
        metodoEdit->addItem(QString());
        metodoEdit->addItem(QString());
        metodoEdit->addItem(QString());
        metodoEdit->setObjectName(QStringLiteral("metodoEdit"));

        gridLayout_14->addWidget(metodoEdit, 2, 1, 1, 1);

        tempoViragemEdit = new QLineEdit(pageInsertConfig);
        tempoViragemEdit->setObjectName(QStringLiteral("tempoViragemEdit"));

        gridLayout_14->addWidget(tempoViragemEdit, 3, 1, 1, 1);

        nSensoresEdit = new QComboBox(pageInsertConfig);
        nSensoresEdit->addItem(QString());
        nSensoresEdit->addItem(QString());
        nSensoresEdit->addItem(QString());
        nSensoresEdit->addItem(QString());
        nSensoresEdit->addItem(QString());
        nSensoresEdit->addItem(QString());
        nSensoresEdit->setObjectName(QStringLiteral("nSensoresEdit"));

        gridLayout_14->addWidget(nSensoresEdit, 1, 1, 1, 1);

        label_20 = new QLabel(pageInsertConfig);
        label_20->setObjectName(QStringLiteral("label_20"));

        gridLayout_14->addWidget(label_20, 2, 0, 1, 1);

        label_19 = new QLabel(pageInsertConfig);
        label_19->setObjectName(QStringLiteral("label_19"));

        gridLayout_14->addWidget(label_19, 3, 0, 1, 1);

        label_17 = new QLabel(pageInsertConfig);
        label_17->setObjectName(QStringLiteral("label_17"));

        gridLayout_14->addWidget(label_17, 0, 0, 1, 1);

        nomeConfigEdit = new QLineEdit(pageInsertConfig);
        nomeConfigEdit->setObjectName(QStringLiteral("nomeConfigEdit"));

        gridLayout_14->addWidget(nomeConfigEdit, 0, 1, 1, 1);

        label_18 = new QLabel(pageInsertConfig);
        label_18->setObjectName(QStringLiteral("label_18"));

        gridLayout_14->addWidget(label_18, 1, 0, 1, 1);

        buttonBoxConfig = new QDialogButtonBox(pageInsertConfig);
        buttonBoxConfig->setObjectName(QStringLiteral("buttonBoxConfig"));
        buttonBoxConfig->setStandardButtons(QDialogButtonBox::Cancel|QDialogButtonBox::Reset|QDialogButtonBox::Save);

        gridLayout_14->addWidget(buttonBoxConfig, 4, 0, 1, 2);

        stackedWidget->addWidget(pageInsertConfig);
        pageProva = new QWidget();
        pageProva->setObjectName(QStringLiteral("pageProva"));
        pushEsquerda = new QPushButton(pageProva);
        pushEsquerda->setObjectName(QStringLiteral("pushEsquerda"));
        pushEsquerda->setGeometry(QRect(20, 110, 241, 71));
        pushVentoinha = new QPushButton(pageProva);
        pushVentoinha->setObjectName(QStringLiteral("pushVentoinha"));
        pushVentoinha->setGeometry(QRect(290, 180, 241, 71));
        pushParado = new QPushButton(pageProva);
        pushParado->setObjectName(QStringLiteral("pushParado"));
        pushParado->setGeometry(QRect(290, 40, 241, 71));
        pushLED = new QPushButton(pageProva);
        pushLED->setObjectName(QStringLiteral("pushLED"));
        pushLED->setGeometry(QRect(290, 110, 241, 71));
        pushDireita = new QPushButton(pageProva);
        pushDireita->setObjectName(QStringLiteral("pushDireita"));
        pushDireita->setGeometry(QRect(20, 40, 241, 71));
        pushFrente = new QPushButton(pageProva);
        pushFrente->setObjectName(QStringLiteral("pushFrente"));
        pushFrente->setGeometry(QRect(20, 180, 241, 71));
        pushButton = new QPushButton(pageProva);
        pushButton->setObjectName(QStringLiteral("pushButton"));
        pushButton->setGeometry(QRect(240, 10, 75, 23));
        stackedWidget->addWidget(pageProva);

        gridLayout->addWidget(stackedWidget, 0, 0, 1, 1);

        MainWindow->setCentralWidget(centralWidget);
        menuBar = new QMenuBar(MainWindow);
        menuBar->setObjectName(QStringLiteral("menuBar"));
        menuBar->setGeometry(QRect(0, 0, 588, 21));
        menuMenu = new QMenu(menuBar);
        menuMenu->setObjectName(QStringLiteral("menuMenu"));
        menuInserir = new QMenu(menuMenu);
        menuInserir->setObjectName(QStringLiteral("menuInserir"));
        menuModificar = new QMenu(menuMenu);
        menuModificar->setObjectName(QStringLiteral("menuModificar"));
        menuEliminar = new QMenu(menuMenu);
        menuEliminar->setObjectName(QStringLiteral("menuEliminar"));
        menuListar = new QMenu(menuMenu);
        menuListar->setObjectName(QStringLiteral("menuListar"));
        menuListar_Detalhado = new QMenu(menuMenu);
        menuListar_Detalhado->setObjectName(QStringLiteral("menuListar_Detalhado"));
        menuModo_Prova = new QMenu(menuBar);
        menuModo_Prova->setObjectName(QStringLiteral("menuModo_Prova"));
        MainWindow->setMenuBar(menuBar);
        statusBar = new QStatusBar(MainWindow);
        statusBar->setObjectName(QStringLiteral("statusBar"));
        MainWindow->setStatusBar(statusBar);

        menuBar->addAction(menuMenu->menuAction());
        menuBar->addAction(menuModo_Prova->menuAction());
        menuMenu->addAction(menuListar_Detalhado->menuAction());
        menuMenu->addAction(menuListar->menuAction());
        menuMenu->addAction(menuInserir->menuAction());
        menuMenu->addAction(menuModificar->menuAction());
        menuMenu->addAction(menuEliminar->menuAction());
        menuInserir->addAction(actionInsProva);
        menuInserir->addAction(actionInsRobot);
        menuInserir->addAction(actionInsEquipa);
        menuInserir->addAction(actionInsConfig);
        menuModificar->addAction(actionModProva);
        menuModificar->addAction(actionModRobot);
        menuModificar->addAction(actionModEquipa);
        menuModificar->addAction(actionModConfig);
        menuEliminar->addAction(actionEliProva);
        menuEliminar->addAction(actionEliRobot);
        menuEliminar->addAction(actionEliEquipa);
        menuEliminar->addAction(actionEliConfig);
        menuListar->addAction(actionListarProva);
        menuListar->addAction(actionListarRobot);
        menuListar->addAction(actionListarEquipa);
        menuListar->addAction(actionLisConfi);
        menuListar_Detalhado->addAction(actionDetProva);
        menuListar_Detalhado->addAction(actionDetEquipa);
        menuListar_Detalhado->addAction(actionDetRobot);
        menuListar_Detalhado->addAction(actionDetConfig);
        menuModo_Prova->addAction(actionIniciar_Prova);

        retranslateUi(MainWindow);

        stackedWidget->setCurrentIndex(13);


        QMetaObject::connectSlotsByName(MainWindow);
    } // setupUi

    void retranslateUi(QMainWindow *MainWindow)
    {
        MainWindow->setWindowTitle(QApplication::translate("MainWindow", "MainWindow", nullptr));
        actionInsProva->setText(QApplication::translate("MainWindow", "Prova", nullptr));
#ifndef QT_NO_STATUSTIP
        actionInsProva->setStatusTip(QApplication::translate("MainWindow", "Inserir uma prova.", nullptr));
#endif // QT_NO_STATUSTIP
        actionModProva->setText(QApplication::translate("MainWindow", "Prova", nullptr));
#ifndef QT_NO_STATUSTIP
        actionModProva->setStatusTip(QApplication::translate("MainWindow", "Modificar uma prova.", nullptr));
#endif // QT_NO_STATUSTIP
        actionEliProva->setText(QApplication::translate("MainWindow", "Prova", nullptr));
#ifndef QT_NO_STATUSTIP
        actionEliProva->setStatusTip(QApplication::translate("MainWindow", "Eliminar uma prova.", nullptr));
#endif // QT_NO_STATUSTIP
        actionInsRobot->setText(QApplication::translate("MainWindow", "Robot", nullptr));
#ifndef QT_NO_STATUSTIP
        actionInsRobot->setStatusTip(QApplication::translate("MainWindow", "Inserir um robot.", nullptr));
#endif // QT_NO_STATUSTIP
        actionModRobot->setText(QApplication::translate("MainWindow", "Robot", nullptr));
#ifndef QT_NO_STATUSTIP
        actionModRobot->setStatusTip(QApplication::translate("MainWindow", "Modificar um robot.", nullptr));
#endif // QT_NO_STATUSTIP
        actionEliRobot->setText(QApplication::translate("MainWindow", "Robot", nullptr));
#ifndef QT_NO_STATUSTIP
        actionEliRobot->setStatusTip(QApplication::translate("MainWindow", "Eliminar um robot.", nullptr));
#endif // QT_NO_STATUSTIP
        actionListarProva->setText(QApplication::translate("MainWindow", "Prova", nullptr));
#ifndef QT_NO_STATUSTIP
        actionListarProva->setStatusTip(QApplication::translate("MainWindow", "Listar todas as provas.", nullptr));
#endif // QT_NO_STATUSTIP
        actionListarRobot->setText(QApplication::translate("MainWindow", "Robot", nullptr));
#ifndef QT_NO_STATUSTIP
        actionListarRobot->setStatusTip(QApplication::translate("MainWindow", "Listar todos os robots.", nullptr));
#endif // QT_NO_STATUSTIP
        actionListarEquipa->setText(QApplication::translate("MainWindow", "Equipa", nullptr));
#ifndef QT_NO_STATUSTIP
        actionListarEquipa->setStatusTip(QApplication::translate("MainWindow", "Listar todas as equipas.", nullptr));
#endif // QT_NO_STATUSTIP
        actionInsEquipa->setText(QApplication::translate("MainWindow", "Equipa", nullptr));
#ifndef QT_NO_STATUSTIP
        actionInsEquipa->setStatusTip(QApplication::translate("MainWindow", "Inserir uma equipa.", nullptr));
#endif // QT_NO_STATUSTIP
        actionModEquipa->setText(QApplication::translate("MainWindow", "Equipa", nullptr));
#ifndef QT_NO_STATUSTIP
        actionModEquipa->setStatusTip(QApplication::translate("MainWindow", "Modificar uma equipa.", nullptr));
#endif // QT_NO_STATUSTIP
        actionEliEquipa->setText(QApplication::translate("MainWindow", "Equipa", nullptr));
#ifndef QT_NO_STATUSTIP
        actionEliEquipa->setStatusTip(QApplication::translate("MainWindow", "Eliminar uma equipa.", nullptr));
#endif // QT_NO_STATUSTIP
        actionDetProva->setText(QApplication::translate("MainWindow", "Prova", nullptr));
#ifndef QT_NO_STATUSTIP
        actionDetProva->setStatusTip(QApplication::translate("MainWindow", "Tabela de Provas.", nullptr));
#endif // QT_NO_STATUSTIP
        actionDetEquipa->setText(QApplication::translate("MainWindow", "Equipa", nullptr));
#ifndef QT_NO_STATUSTIP
        actionDetEquipa->setStatusTip(QApplication::translate("MainWindow", "Tabela de Equipas.", nullptr));
#endif // QT_NO_STATUSTIP
        actionDetRobot->setText(QApplication::translate("MainWindow", "Robot", nullptr));
#ifndef QT_NO_STATUSTIP
        actionDetRobot->setStatusTip(QApplication::translate("MainWindow", "Tabela de Robots.", nullptr));
#endif // QT_NO_STATUSTIP
        actionLisConfi->setText(QApplication::translate("MainWindow", "Configura\303\247\303\243o", nullptr));
#ifndef QT_NO_STATUSTIP
        actionLisConfi->setStatusTip(QApplication::translate("MainWindow", "Listar todas as configura\303\247\303\265es.", nullptr));
#endif // QT_NO_STATUSTIP
        actionDetConfig->setText(QApplication::translate("MainWindow", "Configura\303\247\303\243o", nullptr));
#ifndef QT_NO_STATUSTIP
        actionDetConfig->setStatusTip(QApplication::translate("MainWindow", "Tabela de Configura\303\247\303\265es.", nullptr));
#endif // QT_NO_STATUSTIP
        actionInsConfig->setText(QApplication::translate("MainWindow", "Configura\303\247\303\243o", nullptr));
#ifndef QT_NO_STATUSTIP
        actionInsConfig->setStatusTip(QApplication::translate("MainWindow", "Inserir uma configura\303\247\303\243o.", nullptr));
#endif // QT_NO_STATUSTIP
        actionModConfig->setText(QApplication::translate("MainWindow", "Configura\303\247\303\243o", nullptr));
#ifndef QT_NO_STATUSTIP
        actionModConfig->setStatusTip(QApplication::translate("MainWindow", "Modificar uma configura\303\247\303\243o.", nullptr));
#endif // QT_NO_STATUSTIP
        actionEliConfig->setText(QApplication::translate("MainWindow", "Configura\303\247\303\243o", nullptr));
#ifndef QT_NO_STATUSTIP
        actionEliConfig->setStatusTip(QApplication::translate("MainWindow", "Eliminar uma configura\303\247\303\243o.", nullptr));
#endif // QT_NO_STATUSTIP
        actionIniciar_Prova->setText(QApplication::translate("MainWindow", "Iniciar", nullptr));
#ifndef QT_NO_STATUSTIP
        actionIniciar_Prova->setStatusTip(QApplication::translate("MainWindow", "Iniciar prova.", nullptr));
#endif // QT_NO_STATUSTIP
        label->setText(QApplication::translate("MainWindow", "<html><head/><body><p><span style=\" font-size:18pt;\">LSIS1 - Robot Bombeiro</span></p></body></html>", nullptr));
        QTableWidgetItem *___qtablewidgetitem = tableProva->horizontalHeaderItem(0);
        ___qtablewidgetitem->setText(QApplication::translate("MainWindow", "Nome Prova", nullptr));
        QTableWidgetItem *___qtablewidgetitem1 = tableProva->horizontalHeaderItem(1);
        ___qtablewidgetitem1->setText(QApplication::translate("MainWindow", "Local Prova", nullptr));
        QTableWidgetItem *___qtablewidgetitem2 = tableProva->horizontalHeaderItem(2);
        ___qtablewidgetitem2->setText(QApplication::translate("MainWindow", "Data Prova", nullptr));
        label_2->setText(QApplication::translate("MainWindow", "<html><head/><body><p><span style=\" font-size:12pt;\">Nome Prova:</span></p></body></html>", nullptr));
        label_3->setText(QApplication::translate("MainWindow", "<html><head/><body><p><span style=\" font-size:12pt;\">Local Prova:</span></p></body></html>", nullptr));
        label_4->setText(QApplication::translate("MainWindow", "<html><head/><body><p><span style=\" font-size:12pt;\">Data Prova</span></p></body></html>", nullptr));
        QTableWidgetItem *___qtablewidgetitem3 = tableRobot->horizontalHeaderItem(0);
        ___qtablewidgetitem3->setText(QApplication::translate("MainWindow", "Nome", nullptr));
        QTableWidgetItem *___qtablewidgetitem4 = tableRobot->horizontalHeaderItem(1);
        ___qtablewidgetitem4->setText(QApplication::translate("MainWindow", "Equipa", nullptr));
        QTableWidgetItem *___qtablewidgetitem5 = tableRobot->horizontalHeaderItem(2);
        ___qtablewidgetitem5->setText(QApplication::translate("MainWindow", "Configura\303\247\303\243o", nullptr));
        label_5->setText(QApplication::translate("MainWindow", "<html><head/><body><p><span style=\" font-size:12pt;\">Nome robot:</span></p></body></html>", nullptr));
        label_6->setText(QApplication::translate("MainWindow", "<html><head/><body><p><span style=\" font-size:12pt;\">Equipa:</span></p></body></html>", nullptr));
        label_7->setText(QApplication::translate("MainWindow", "<html><head/><body><p><span style=\" font-size:12pt;\">Configura\303\247\303\243o</span></p></body></html>", nullptr));
        QTableWidgetItem *___qtablewidgetitem6 = tableEquipa->horizontalHeaderItem(0);
        ___qtablewidgetitem6->setText(QApplication::translate("MainWindow", "Nome", nullptr));
        QTableWidgetItem *___qtablewidgetitem7 = tableEquipa->horizontalHeaderItem(1);
        ___qtablewidgetitem7->setText(QApplication::translate("MainWindow", "Nome Capit\303\243o", nullptr));
        QTableWidgetItem *___qtablewidgetitem8 = tableEquipa->horizontalHeaderItem(2);
        ___qtablewidgetitem8->setText(QApplication::translate("MainWindow", "Nome Elemento 2", nullptr));
        QTableWidgetItem *___qtablewidgetitem9 = tableEquipa->horizontalHeaderItem(3);
        ___qtablewidgetitem9->setText(QApplication::translate("MainWindow", "Nome Elemento 3", nullptr));
        QTableWidgetItem *___qtablewidgetitem10 = tableEquipa->horizontalHeaderItem(4);
        ___qtablewidgetitem10->setText(QApplication::translate("MainWindow", "Nome Elemento 4", nullptr));
        QTableWidgetItem *___qtablewidgetitem11 = tableEquipa->horizontalHeaderItem(5);
        ___qtablewidgetitem11->setText(QApplication::translate("MainWindow", "Nome Elemento 5", nullptr));
        QTableWidgetItem *___qtablewidgetitem12 = tableEquipa->horizontalHeaderItem(6);
        ___qtablewidgetitem12->setText(QApplication::translate("MainWindow", "Nome Elemento 6", nullptr));
        QTableWidgetItem *___qtablewidgetitem13 = tableEquipa->horizontalHeaderItem(7);
        ___qtablewidgetitem13->setText(QApplication::translate("MainWindow", "Nome Elemento 7", nullptr));
        QTableWidgetItem *___qtablewidgetitem14 = tableEquipa->horizontalHeaderItem(8);
        ___qtablewidgetitem14->setText(QApplication::translate("MainWindow", "Nome Elemento 8", nullptr));
        label_9->setText(QApplication::translate("MainWindow", "<html><head/><body><p><span style=\" font-size:10pt;\">Nome Elemento:</span></p></body></html>", nullptr));
        label_11->setText(QApplication::translate("MainWindow", "<html><head/><body><p><span style=\" font-size:10pt;\">Nome Elemento:</span></p></body></html>", nullptr));
        label_16->setText(QApplication::translate("MainWindow", "<html><head/><body><p><span style=\" font-size:10pt;\">Nome Elemento:</span></p></body></html>", nullptr));
        label_15->setText(QApplication::translate("MainWindow", "<html><head/><body><p><span style=\" font-size:10pt;\">Nome Elemento:</span></p></body></html>", nullptr));
        label_12->setText(QApplication::translate("MainWindow", "<html><head/><body><p><span style=\" font-size:10pt;\">Nome Elemento:</span></p></body></html>", nullptr));
        label_14->setText(QApplication::translate("MainWindow", "<html><head/><body><p><span style=\" font-size:10pt;\">Nome Elemento:</span></p></body></html>", nullptr));
        label_8->setText(QApplication::translate("MainWindow", "<html><head/><body><p><span style=\" font-size:10pt;\">Nome Equipa:</span></p></body></html>", nullptr));
        label_13->setText(QApplication::translate("MainWindow", "<html><head/><body><p><span style=\" font-size:10pt;\">Nome Elemento:</span></p></body></html>", nullptr));
        label_10->setText(QApplication::translate("MainWindow", "<html><head/><body><p><span style=\" font-size:10pt;\">Nome Elemento:</span></p></body></html>", nullptr));
        QTableWidgetItem *___qtablewidgetitem15 = tableConfig->horizontalHeaderItem(0);
        ___qtablewidgetitem15->setText(QApplication::translate("MainWindow", "Nome Configuracao", nullptr));
        QTableWidgetItem *___qtablewidgetitem16 = tableConfig->horizontalHeaderItem(1);
        ___qtablewidgetitem16->setText(QApplication::translate("MainWindow", "Metodo Extin\303\247\303\243o", nullptr));
        QTableWidgetItem *___qtablewidgetitem17 = tableConfig->horizontalHeaderItem(2);
        ___qtablewidgetitem17->setText(QApplication::translate("MainWindow", "NumeroSenrores", nullptr));
        QTableWidgetItem *___qtablewidgetitem18 = tableConfig->horizontalHeaderItem(3);
        ___qtablewidgetitem18->setText(QApplication::translate("MainWindow", "Tempo Viragem", nullptr));
        metodoEdit->setItemText(0, QApplication::translate("MainWindow", "Ventoinha", nullptr));
        metodoEdit->setItemText(1, QApplication::translate("MainWindow", "Agua", nullptr));
        metodoEdit->setItemText(2, QApplication::translate("MainWindow", "Gas", nullptr));

        nSensoresEdit->setItemText(0, QApplication::translate("MainWindow", "1", nullptr));
        nSensoresEdit->setItemText(1, QApplication::translate("MainWindow", "2", nullptr));
        nSensoresEdit->setItemText(2, QApplication::translate("MainWindow", "3", nullptr));
        nSensoresEdit->setItemText(3, QApplication::translate("MainWindow", "4", nullptr));
        nSensoresEdit->setItemText(4, QApplication::translate("MainWindow", "5", nullptr));
        nSensoresEdit->setItemText(5, QApplication::translate("MainWindow", "6", nullptr));

        label_20->setText(QApplication::translate("MainWindow", "<html><head/><body><p><span style=\" font-size:11pt;\">Metodo Extin\303\247\303\243o:</span></p></body></html>", nullptr));
        label_19->setText(QApplication::translate("MainWindow", "<html><head/><body><p><span style=\" font-size:11pt;\">Tempo Viragem:</span></p></body></html>", nullptr));
        label_17->setText(QApplication::translate("MainWindow", "<html><head/><body><p><span style=\" font-size:11pt;\">Nome Configuracao:</span></p></body></html>", nullptr));
        label_18->setText(QApplication::translate("MainWindow", "<html><head/><body><p><span style=\" font-size:11pt;\">Numero Sensores:</span></p></body></html>", nullptr));
        pushEsquerda->setText(QApplication::translate("MainWindow", "Esquerda", nullptr));
        pushVentoinha->setText(QApplication::translate("MainWindow", "Ventoinha", nullptr));
        pushParado->setText(QApplication::translate("MainWindow", "Parar", nullptr));
        pushLED->setText(QApplication::translate("MainWindow", "LED", nullptr));
        pushDireita->setText(QApplication::translate("MainWindow", "Direita", nullptr));
        pushFrente->setText(QApplication::translate("MainWindow", "Frente", nullptr));
        pushButton->setText(QApplication::translate("MainWindow", "Iniciar", nullptr));
        menuMenu->setTitle(QApplication::translate("MainWindow", "Menu", nullptr));
        menuInserir->setTitle(QApplication::translate("MainWindow", "Inserir", nullptr));
        menuModificar->setTitle(QApplication::translate("MainWindow", "Modificar", nullptr));
        menuEliminar->setTitle(QApplication::translate("MainWindow", "Eliminar", nullptr));
        menuListar->setTitle(QApplication::translate("MainWindow", "Listar", nullptr));
        menuListar_Detalhado->setTitle(QApplication::translate("MainWindow", "Listar Detalhado", nullptr));
        menuModo_Prova->setTitle(QApplication::translate("MainWindow", "Modo Prova", nullptr));
    } // retranslateUi

};

namespace Ui {
    class MainWindow: public Ui_MainWindow {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_MAINWINDOW_H
