#include <Ultrasonic.h>
#include <SoftwareSerial.h>


SoftwareSerial BTSerial(2, 10);
//bluetooth 0 rx   e 1 tx

// BOTAO VERDE A3
// BOTÃO VERMELHO A4

int estado;
int BluetoothData;
int t90 = 2900 / 2; //tempo de viragem de 90 graus
int t90r = t90 / 2;

int x = 0; //variável caso nao detete vela

int fase = 0;

int trigPinf = 8;
int trigPine = 4;
int trigPind = 6;
int echoPinf = 9;
int echoPine = 5;
int echoPind = 7;

int pinverde = A3;
int pinvermelho = A4;

int pinventoinha = A2;
int pinled = 1;
int pinsensor = A5; // sensor calor
int sensorValue;
boolean iniciar = false;

void setup() {
  //Serial.begin (9600);
  //Serial.println ("A ler dados do sensor");

  //ligação bluetooth
  BTSerial.begin(9600);
  pinMode (trigPind, OUTPUT);
  pinMode (echoPind, INPUT);
  pinMode (trigPine, OUTPUT);
  pinMode (echoPine, INPUT);
  pinMode (trigPinf, OUTPUT);
  pinMode (echoPinf, INPUT);

  pinMode(12, OUTPUT);
  pinMode(3, OUTPUT);
  pinMode(13, OUTPUT);
  pinMode(11, OUTPUT);

  pinMode (pinled, OUTPUT);
  pinMode (pinventoinha, OUTPUT);
  pinMode (pinsensor, INPUT);

  digitalWrite (1, HIGH);
}


int lercalor() {
  sensorValue = analogRead(pinsensor);
  return sensorValue;

}

int lersensorf () {
  digitalWrite(trigPinf, LOW);
  delayMicroseconds(5);
  digitalWrite(trigPinf, HIGH);

  double tempof = pulseIn(echoPinf, HIGH);
  double distanciaf = tempof * 0.034 / 2;
//  Serial.println(">Sensor frente");
    BTSerial.println(distanciaf);
  return distanciaf;
}

int lersensore() {
  // Ler sonar lado esquerdo
  digitalWrite(trigPine, LOW);
  delayMicroseconds(5);
  digitalWrite(trigPine, HIGH);

  double tempoe = pulseIn(echoPine, HIGH);
  double distanciae = tempoe * 0.034 / 2;
//  Serial.println(">Sensor lado esq");
//  Serial.println(distanciae);
  return distanciae;
}

// Ler sonar lado direito
int lersensord() {
  digitalWrite(trigPind, LOW);
  delayMicroseconds(5);
  digitalWrite(trigPind, HIGH);

  double tempod = pulseIn(echoPind, HIGH);
  double distanciad = tempod * 0.034 / 2;
//  Serial.println(">Sensor lado dir");
//  Serial.println(distanciad);
  return distanciad;
}

void setMotorA (unsigned char dir, int pwm) {
  analogWrite (3, pwm);
  digitalWrite (12, dir);
}

void setMotorB(unsigned char dir, int pwm) {
  analogWrite (11, pwm);
  digitalWrite (13, dir);
}

void andarFrente (int vel) {
  setMotorA (HIGH, vel);
  setMotorB (HIGH, vel);
  Serial.println(2);
  BTSerial.println(2);
}


void virarDireita (int vel) {
  setMotorA (HIGH, vel);
  setMotorB( LOW, vel);
  Serial.println(3);
  BTSerial.println(3);
}

void rodar () {
  setMotorA (HIGH, 255);
  setMotorB (LOW, 255);
  delay (1000);
  x = 1;
}

void virarEsquerda (int vel) {
  setMotorA(LOW, vel);
  setMotorB (HIGH, vel);
  BTSerial.println(4);
  Serial.println(4);
}

void parar () {
  setMotorA (HIGH, 0);
  setMotorB (HIGH, 0);
  delay(1000);
  Serial.println(1);
  BTSerial.println(1);
}

void ligarVentoinha () {
  digitalWrite (pinventoinha, HIGH);
  Serial.println(5);
  BTSerial.println(5);

}

void desligarVentoinha () {
  digitalWrite (pinventoinha, LOW);
}


void loop() {
  sensorValue = analogRead(pinsensor);
  //Serial.println(sensorValue);

  //criar fases, e fazer o swtich em função da fase, meter whiles para verificar as distancias
  digitalWrite(trigPinf, LOW);
  delayMicroseconds(5);
  digitalWrite(trigPinf, HIGH);

  double tempof = pulseIn(echoPinf, HIGH);
  double distanciaf = tempof * 0.034 / 2;
  //BTSerial.println(">Sensor frente");
  //BTSerial.println(distanciaf);

  // Ler sonar lado esquerdo
  digitalWrite(trigPine, LOW);
  delayMicroseconds(5);
  digitalWrite(trigPine, HIGH);

  double tempoe = pulseIn(echoPine, HIGH);
  double distanciae = tempoe * 0.034 / 2;
  // BTSerial.println(">Sensor lado esq");
  //BTSerial.println(distanciae);

  // Ler sonar lado direito
  digitalWrite(trigPind, LOW);
  delayMicroseconds(5);
  digitalWrite(trigPind, HIGH);

  double tempod = pulseIn(echoPind, HIGH);
  double distanciad = tempod * 0.034 / 2;
  // BTSerial.println(">Sensor lado dir");
  //BTSerial.println(distanciad);

//if (Serial.available()){
//    char fase_indicacao = Serial.read();
//    int faseInt = Serial.parseInt();
//    Serial.println(faseInt);
//    if(faseInt != 0){
//       digitalWrite(pinverde,HIGH);
//        fase = 1;
//    }
//  }

  if (!iniciar) {
    while (digitalRead(pinverde) == HIGH);
    iniciar = true;
    //Serial.println("Comecar");
    fase = 0 ;
  }

  if (digitalRead(pinvermelho) == LOW) {
    parar();
    iniciar = false;
    //Serial.println("Parar");
    fase = -1;
  }

  switch (fase) {
      int dist;
    case -1:
      parar();
      break;
      
    case -2:
    if (lersensore() < 30) {
      virarEsquerda(190);
      delay (t90);
      fase = 0;
      
    }
    else {
     
      fase =0;
    }
    break;
    
    case 0:
      do {
        andarFrente(255);
        delay(1000);
      } while (lersensord() < 30);
      fase = 1;
     // BTSerial.println ("Corredor -> curva ");
      break;

    case 1:
      virarDireita (255);
      delay (t90);
      //BTSerial.println ("-> corredor quarto 3");
      fase = 2;
      break;

    case 2:
      do {
        andarFrente(190);
      } while (lersensorf() > 10);
      fase = 3;
      break;

    case 3:
      virarDireita (255);
      delay (t90);
      fase = 4;
      parar();
      //BTSerial.println ("Dentro Quarto");
      break;

    case 4:
      do {
        andarFrente(190);
      } while (lersensorf() > 10);
      fase = 5;
      break;

    case 5:
      //BTSerial.println ("Canto 1 Q3 ");
      if (lercalor() > 500) {
        parar();
        digitalWrite (pinled, HIGH);
        delay(3000);
        do {
          ligarVentoinha();
          delay(5000);
        } while (lercalor() > 50);
        digitalWrite (pinled, LOW);
      } else {
        desligarVentoinha();
        fase = 6;
      }
      break;
    case 6 :
      virarDireita (255);
      delay (t90);
      fase = 7;
      parar();
      break;

    case 7:
      do {
        andarFrente(190);
        Serial.println(lersensorf());
      } while (lersensorf() > 10);
      fase = 8;
      BTSerial.println ("Q2");
      break;

    case 8:
      if (lercalor() > 500) {
        parar();
        digitalWrite (pinled, HIGH);
        delay(3000);
        do {
          ligarVentoinha();
          delay(5000);
        } while (lercalor() > 50);
        digitalWrite (pinled, LOW);
      } else {
        desligarVentoinha();
        fase = 9;
      }
      break;

    case 9:
      virarDireita (255);
      delay (t90);
      fase = 10;
      parar();
      break;

    case 10:
      do {
        andarFrente(190);
        Serial.println(lersensorf());
      } while (lersensorf() > 10);
      fase = 11;
      BTSerial.println ("Q3");
      break;

    case 11:
      if (lercalor() > 500) {
        parar();
        digitalWrite (pinled, HIGH);
        delay(3000);
        do {
          ligarVentoinha();
          delay(5000);
        } while (lercalor() > 50);
        digitalWrite (pinled, LOW);
      } else {
        desligarVentoinha();
        fase = 12;
      }
      break;

    case 12:
      virarDireita (255);
      delay (t90);
      fase = 13;
      parar();
      break;

    case 13:
      do {
        andarFrente(190);
        Serial.println(lersensorf());
      } while (lersensorf() > 10);
      fase = 14;
      break;

    case 14:
      virarEsquerda (255);
      delay (t90);
      fase = 15;
      parar();
      break;

    case 15:
      do {
        andarFrente(190);
        Serial.println(lersensorf());
      } while (lersensorf() > 10);
      fase = 16;
      break;

    case 16:
      virarEsquerda (255);
      delay (t90);
      fase = 17;
      parar();
      BTSerial.println ("Corredor sair");
      break;

    // FIM QUARTO 1

    // Quarto 2
    case 17:
      do {
        andarFrente(255);
        delay(1000);
      } while (lersensord() < 30);
      fase = 18;
      BTSerial.println ("Corredo-> virar Cruzamento");
      break;

    case 18:
      virarDireita (255);
      delay (t90);
      fase = 19;
      break;

    case 19:
      do {
        andarFrente(190);
        Serial.println(lersensorf());
      } while (lersensorf() > 10);
      fase = 20;
      break;

    case 20:
      virarDireita (255);
      delay (t90);
      fase = 1000;
      break;

    case 1000:
      do {
        andarFrente(190);
        Serial.println(lersensorf());
      } while (lersensorf() > 10);
      fase = 21;
      break;

    case 21:
      //BTSerial.println ("Canto 1 Q3 ");
      if (lercalor() > 500) {
        parar();
        digitalWrite (pinled, HIGH);
        delay(3000);
        do {
          ligarVentoinha();
          delay(5000);
        } while (lercalor() > 50);
        digitalWrite (pinled, LOW);
      } else {
        desligarVentoinha();
        fase = 22;
      }
      break;
      
    case 22:
      virarDireita (255);
      delay (t90);
      fase = 23;
      break;

    case 23:
      do {
        andarFrente(190);
        Serial.println(lersensorf());
      } while (lersensorf() > 10);
      fase = 24;
      break;

    case 24:
      if (lercalor() > 500) {
        digitalWrite (pinled, HIGH);
        delay(3000);
        do {
          ligarVentoinha();
          delay(5000);
        } while (lercalor() > 50);
        digitalWrite (pinled, LOW);
        fase = 25;
      } else {
        desligarVentoinha();
        fase = 25;
      }
      break;

    case 25:
      virarDireita (255);
      delay (t90);
      fase = 26;
      break;

    case 26:
      do {
        andarFrente(190);
        Serial.println(lersensorf());
      } while (lersensorf() > 10);
      fase = 27;
      break;

    case 27:
      if (lercalor() > 500) {
        parar();
        digitalWrite (pinled, HIGH);
        delay(3000);
        do {
          ligarVentoinha();
          delay(5000);
        } while (lercalor() > 50);
        digitalWrite (pinled, LOW);
      } else {
        desligarVentoinha();
        fase = 28;
      }
      break;

    case 28:
      virarDireita (255);
      delay (t90);
      fase = 29;
      break;

    case 29:
      do {
        andarFrente(190);
        Serial.println(lersensorf());
      } while (lersensorf() > 10);
      fase = 30;
      break;

    case 30:
      virarEsquerda(255);
      delay (t90);
      fase = 310;
      break;

    case 310:
      do {
        andarFrente(190);
        Serial.println(lersensorf());
      } while (lersensorf() > 10);
      fase = 302;
      break;

    case 302:
      virarEsquerda(255);
      delay (t90);
      fase = 31;
      break;
    // FIM QUARTO 2

    // QUARTO 3
    case 31:
      if (lersensord() < 30 ) {
        andarFrente(205);
        delay(1200);
      }
      else   {
        fase = 32;
      }
      break;

    case 32:
      virarDireita (255);
      delay (t90);
      parar();
      fase = 33;
      break;

    case 33:
      do {
        andarFrente(190);
        Serial.println(lersensorf());
      } while (lersensorf() > 10);
      fase = 34;
      break;

    case 34:
      virarDireita (255);
      delay (t90);
      parar();
      fase = 35;
      break;

    case 35:
      do {
        andarFrente(190);
      } while (lersensorf() > 10);
      //Serial.println(dist);
      //BTSerial.println("Canto 1, Q1");
      fase = 36;
      break;

    case 36:
      if (lercalor() > 500) {
        parar();
        digitalWrite (pinled, HIGH);
        delay(3000);
        do {
          ligarVentoinha();
          delay(5000);
        } while (lercalor() > 50);
        digitalWrite (pinled, LOW);
      } else {
        desligarVentoinha();
        fase = 37;
      }
      break;

    case 37:
      virarDireita (255);
      delay (t90);
      parar();
      fase = 38;
      break;

    case 38:
      do {
        andarFrente(190);
        Serial.println(lersensorf());
      } while (lersensorf() > 10);
      fase = 39;
      break;

    case 39:
      if (lercalor() > 500) {
        parar();
        digitalWrite (pinled, HIGH);
        delay(3000);
        do {
          ligarVentoinha();
          delay(5000);
        } while (lercalor() > 50);
        digitalWrite (pinled, LOW);
      } else {
        desligarVentoinha();
        fase = 41;
      }
      break;

    case 41:
      virarDireita (255);
      delay (t90);
      fase = 42;
      parar();
      break;

    case 42:
      do {
        andarFrente(190);
        Serial.println(lersensorf());
      } while (lersensorf() > 10);
      fase = 43;
      break;

    case 43:
      virarDireita (255);
      delay (t90);
      parar();
      fase = 44;
      break;

    case 44:
      do {
        andarFrente(190);
        Serial.println(lersensorf());
      } while (lersensorf() > 10);
      fase = 45;
      break;

    case 45:
      virarEsquerda (255);
      delay (t90);
      fase = 46;
      parar();
      break;

    case 46:
      do {
        andarFrente(190);
        Serial.println(lersensorf());
      } while (lersensorf() > 10);
      fase = 47;
      break;

    case 47:
      virarEsquerda (255);
      delay (t90);
      fase = 48;
      parar();
      break;
    //FIM QUARTO 3

    // QUARTO 4
    case 48:

        andarFrente(255);
        delay(1500);
      
        fase = 49;
      
      break;

    case 49:
      virarEsquerda (255);
      delay (t90);
      parar();
      fase = 50;
      break;

    case 50:
      do {
        andarFrente(190);
        Serial.println(lersensorf());
      } while (lersensorf() > 10);
      fase = 51;
      break;

    case 51:
      if (lercalor() > 500) {
        parar();
        digitalWrite (pinled, HIGH);
        delay(3000);
        do {
          ligarVentoinha();
          delay(5000);
        } while (lercalor() > 50);
        digitalWrite (pinled, LOW);
      } else {
        desligarVentoinha();
        fase = 52;
      }
      break;

    case 52:
      virarDireita (255);
      delay (t90);
      parar();
      fase = 53;
      break;

    case 53:
      do {
        andarFrente(190);
        Serial.println(lersensorf());
      } while (lersensorf() > 10);
      fase = 54;
      break;

    case 54:
      if (lercalor() > 500) {
        parar();
        digitalWrite (pinled, HIGH);
        delay(3000);
        do {
          ligarVentoinha();
          delay(5000);
        } while (lercalor() > 50);
        digitalWrite (pinled, LOW);
      } else {
        desligarVentoinha();
        fase = 55;
      }
      break;

    case 55:
      virarDireita (255);
      delay (t90);
      parar();
      fase = 56;
      break;

    case 56:
      do {
        andarFrente(190);
        Serial.println(lersensorf());
      } while (lersensorf() > 10);
      fase = 57;
      break;

    case 57:
      if (lercalor() > 500) {
        parar();
        digitalWrite (pinled, HIGH);
        delay(3000);
        do {
          ligarVentoinha();
          delay(5000);
        } while (lercalor() > 50);
        digitalWrite (pinled, LOW);
      } else {
        desligarVentoinha();
        fase = 59;
      }
      break;

    case 59:
      virarDireita (255);
      delay (t90);
      parar();
      fase = 60;
      break;

    case 60:
      do {
        andarFrente(190);
        Serial.println(lersensorf());
      } while (lersensorf() > 10);
      fase = 61;
      break;

    case 61:
      virarEsquerda(255);
      delay (t90);
      fase = 62;
      parar();
      break;

    case 62:
      do {
        andarFrente(190);
        Serial.println(lersensorf());
      } while (lersensorf() > 10);
      fase = 63;
      break;

    case 63:
      virarEsquerda(255);
      delay (t90);
      fase = 64;
      parar();
      break;

    case 64:
      do {
        andarFrente(190);
        Serial.println(lersensorf());
      } while (lersensorf() > 10);
      fase = 65;
      break;

    case 65:
      virarEsquerda (255);
      delay (t90);
      parar();
      fase = -1;
      break;


      if (BTSerial.available()) {

        if (Serial.available())   {
          delay(10);
          BTSerial.write(Serial.read());
        }

      }
  }
}


