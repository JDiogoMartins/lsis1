-- phpMyAdmin SQL Dump
-- version 4.7.9
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: 23-Jun-2018 às 20:26
-- Versão do servidor: 10.1.31-MariaDB
-- PHP Version: 7.2.3

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `lsis1`
--

-- --------------------------------------------------------

--
-- Estrutura da tabela `configuracao`
--

CREATE TABLE `configuracao` (
  `IDConfiguracao` int(11) NOT NULL,
  `NomeConfiguracao` varchar(30) DEFAULT NULL,
  `NumeroSensores` varchar(30) DEFAULT NULL,
  `Metodo` varchar(30) DEFAULT NULL,
  `TempoViragem` varchar(30) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Extraindo dados da tabela `configuracao`
--

INSERT INTO `configuracao` (`IDConfiguracao`, `NomeConfiguracao`, `NumeroSensores`, `Metodo`, `TempoViragem`) VALUES
(1, 'Configuracao 1', '3', 'Ventoinha', '45');

-- --------------------------------------------------------

--
-- Estrutura da tabela `equipa`
--

CREATE TABLE `equipa` (
  `IDEquipa` int(20) NOT NULL,
  `NomeEquipa` varchar(30) DEFAULT NULL,
  `NomeEle1` varchar(30) DEFAULT NULL,
  `NomeEle2` varchar(30) DEFAULT NULL,
  `NomeEle3` varchar(30) DEFAULT NULL,
  `NomeEle4` varchar(30) DEFAULT NULL,
  `NomeEle5` varchar(30) DEFAULT NULL,
  `NomeEle6` varchar(30) DEFAULT NULL,
  `NomeEle7` varchar(30) DEFAULT NULL,
  `NomeEle8` varchar(30) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Extraindo dados da tabela `equipa`
--

INSERT INTO `equipa` (`IDEquipa`, `NomeEquipa`, `NomeEle1`, `NomeEle2`, `NomeEle3`, `NomeEle4`, `NomeEle5`, `NomeEle6`, `NomeEle7`, `NomeEle8`) VALUES
(1, 'Sem equipa', 'Nome Capitão', 'Desconhecido', 'Desconhecido', 'Desconhecido', 'Desconhecido', 'Desconhecido', 'Desconhecido', 'Desconhecido');

-- --------------------------------------------------------

--
-- Estrutura da tabela `prova`
--

CREATE TABLE `prova` (
  `IDProva` int(20) NOT NULL,
  `NomeProva` varchar(30) DEFAULT NULL,
  `LocalProva` varchar(30) DEFAULT NULL,
  `DataProva` varchar(30) DEFAULT '2018-06-12'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Extraindo dados da tabela `prova`
--

INSERT INTO `prova` (`IDProva`, `NomeProva`, `LocalProva`, `DataProva`) VALUES
(1, 'Robot Bombeiro', 'Porto', '12 ago 2018'),
(3, 'Robot Salvador', 'Lisboa', NULL),
(38, 'Robot Taxi', 'Los Angeles', NULL),
(39, 'Nova prova', 'Indefinido', '');

-- --------------------------------------------------------

--
-- Estrutura da tabela `robot`
--

CREATE TABLE `robot` (
  `IDRobot` int(11) NOT NULL,
  `NomeRobot` varchar(30) DEFAULT NULL,
  `IDEquipa` int(11) NOT NULL,
  `IDConfiguracao` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Extraindo dados da tabela `robot`
--

INSERT INTO `robot` (`IDRobot`, `NomeRobot`, `IDEquipa`, `IDConfiguracao`) VALUES
(1, 'Jarvas', 1, 1);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `configuracao`
--
ALTER TABLE `configuracao`
  ADD PRIMARY KEY (`IDConfiguracao`);

--
-- Indexes for table `equipa`
--
ALTER TABLE `equipa`
  ADD PRIMARY KEY (`IDEquipa`) USING BTREE;

--
-- Indexes for table `prova`
--
ALTER TABLE `prova`
  ADD PRIMARY KEY (`IDProva`);

--
-- Indexes for table `robot`
--
ALTER TABLE `robot`
  ADD PRIMARY KEY (`IDRobot`),
  ADD KEY `IDEquipa` (`IDEquipa`) USING BTREE,
  ADD KEY `IDConfiguracao` (`IDConfiguracao`) USING BTREE;

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `configuracao`
--
ALTER TABLE `configuracao`
  MODIFY `IDConfiguracao` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `equipa`
--
ALTER TABLE `equipa`
  MODIFY `IDEquipa` int(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `prova`
--
ALTER TABLE `prova`
  MODIFY `IDProva` int(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=40;

--
-- AUTO_INCREMENT for table `robot`
--
ALTER TABLE `robot`
  MODIFY `IDRobot` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- Constraints for dumped tables
--

--
-- Limitadores para a tabela `robot`
--
ALTER TABLE `robot`
  ADD CONSTRAINT `robot_ibfk_1` FOREIGN KEY (`IDEquipa`) REFERENCES `equipa` (`IDEquipa`),
  ADD CONSTRAINT `robot_ibfk_2` FOREIGN KEY (`IDConfiguracao`) REFERENCES `configuracao` (`IDConfiguracao`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
